import { Component } from '@angular/core';
import { User } from './signupform.interface';

@Component({
  selector: 'app-signupform',
  styleUrls : ['./form.component.css'],
  templateUrl: './form.component.html'
})
export class FormComponent {
  user: User = {
    name: '',
    account: {
      email: '',
      confirm: ''
    }
  };
  onSubmit({ value, valid }: { value: User, valid: boolean }) {
    console.log(value, valid);
  }
}

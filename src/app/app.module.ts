import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { TestComponent} from './test.component';
 import { TodoComponent} from './todo.component';
 import { TodoListComponent} from './todo-list.component';
  import { TodoFormComponent } from './todo-form.component';
import { TodoService } from './todo.service';
 import { TodosComponent} from './todos.component';
 import {FormComponent} from './form.component';
 import {SampleComponent} from './sample.component';
@NgModule({
  declarations: [
    AppComponent ,
    TestComponent ,
    TodoComponent ,
    TodosComponent , TodoFormComponent , TodoListComponent , FormComponent , SampleComponent],
  imports: [
    BrowserModule , FormsModule
  ],
  providers: [TodoService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';

import { TodoService } from './todo.service';
import { Todo } from './todo';

@Component({
  selector: 'app-todo-form-smart',
  templateUrl: './todo-form-smart.component.html'
})
export class TodoFormSmartComponent {

  constructor(private todoService: TodoService) {
   // this.appTodos = this.todoService.getTodos();
  }

  addTodo(item: { label: string }) {
    this.todoService.addTodo(item);
  }

}




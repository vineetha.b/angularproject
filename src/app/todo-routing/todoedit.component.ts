import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Todo } from './todo';


@Component({
  selector: 'todoedit',
  templateUrl: './todoedit.component.html'
})
export class TodoEditComponent implements OnInit {

  @Input() todo: Todo;

  @Output() onUpdate = new EventEmitter();

  constructor() { }

  update() {
    this.onUpdate.emit(this.todo);
  }

  ngOnInit() {
  }

}

import {Component , OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {TodoService} from './todo.service';
@Component({
selector: 'todoeditsmart',
templateUrl: './todoeditsmart.component.html'
})
export class TodoEditSmartComponent implements OnInit {

  selectedTodo;
    constructor(private route: ActivatedRoute, private todoService: TodoService) {}
    ngOnInit() {
      this.route.params.subscribe((params) => {
       const todo =  this.todoService.findById(parseInt(params.id, 10));
        this.selectedTodo = todo;
      });
    }

    updateToDo(todo) {
      this.todoService.update(todo);
    }

  }

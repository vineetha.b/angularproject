import { Component, OnInit } from '@angular/core';
import { TodoService} from './todo.service';
import { Todo } from './todo';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
})
export class TodosComponent {
  appTodos: Todo[];
  constructor(private todoService: TodoService) {
    this.appTodos = this.todoService.getTodos();
  }

  addTodo(item: { label: string }) {
    this.todoService.addTodo(item);
    this.appTodos = this.todoService.getTodos();
  }
  completeTodo(item: { todo: Todo }) {
    this.todoService.completeTodo(item.todo);
    this.appTodos = this.todoService.getTodos();
  }

  // Object.assign({}, item, {complete: true})

  removeTodo(item: { todo: Todo }) {
    this.todoService.removeTodo(item.todo);
    this.appTodos = this.todoService.getTodos();
  }
}

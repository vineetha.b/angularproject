export interface Todo {
  label: string;
  id: number;
  complete: boolean;
}

import {Component , Input , Output , EventEmitter} from '@angular/core';


@Component({
  selector: 'app-test',
  styleUrls: ['test.component.css'],
  templateUrl: './test.component.html'
})
export class TestComponent  {
  @Input('init')
  count= 0;
  @Output('update')
  change: EventEmitter<number> = new EventEmitter<number>();
  increment() {
    this.count++;
    this.change.emit(this.count);
  }
  decrement() {
    this.count--;
    this.change.emit(this.count);
}
}
